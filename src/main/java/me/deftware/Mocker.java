package me.deftware;

import org.objectweb.asm.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

public abstract class Mocker {

    public static final String DELIMITER = ";";
    private static final int JAVA_VERSION = Opcodes.V1_8;
    private static final int ASM_VERSION = Opcodes.ASM9;

    private final Map<String, MockClass> classes = new HashMap<>();

    /**
     * Whether to visit methods called
     */
    private boolean visitMethods = true;
    /**
     * Whether to visit implemented interfaces
     */
    private boolean visitInterfaces = true;
    /**
     * Whether to visit extended super classes
     */
    private boolean visitSuperClasses = true;

    private final File path;

    public Mocker(File path) {
        this.path = path;
    }

    public void run() throws IOException {
        try (JarFile jar = new JarFile(path)) {
            Enumeration<JarEntry> enumeration = jar.entries();
            Visitor visitor = new Visitor();
            while (enumeration.hasMoreElements()) {
                JarEntry entry = enumeration.nextElement();
                String name = entry.getName();
                if (entry.isDirectory() || !name.endsWith(".class")) {
                    continue;
                }
                InputStream stream = jar.getInputStream(entry);
                ClassReader reader = new ClassReader(stream);
                reader.accept(visitor, ClassReader.EXPAND_FRAMES | ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES);
            }
        }
    }

    public void write(Path path) throws Exception {
        write(path, null);
    }

    public void write(Path path, IOConsumer<JarOutputStream> consumer) throws Exception {
        // Create a new jar file, and write all classes to it
        try (OutputStream outStream = Files.newOutputStream(path);
             JarOutputStream jarStream = new JarOutputStream(outStream)) {
            for (Map.Entry<String, MockClass> entry : classes.entrySet()) {
                String name = entry.getKey();
                var visitor = entry.getValue().writer;
                visitor.visitEnd();
                byte[] bytes = visitor.toByteArray();
                jarStream.putNextEntry(new JarEntry(name + ".class"));
                jarStream.write(bytes);
                jarStream.closeEntry();
            }
            if (consumer != null) {
                consumer.accept(jarStream);
            }
        }
    }

    public interface IOConsumer<T> {
        void accept(T t) throws IOException;
    }

    public abstract boolean isMockableMethod(String owner, String name, String descriptor);

    public abstract boolean isMockableClass(String clazz);

    public void setVisitMethods(boolean flag) {
        this.visitMethods = flag;
    }

    public void setVisitInterfaces(boolean flag) {
        this.visitInterfaces = flag;
    }

    public void setVisitSuperClasses(boolean flag) {
        this.visitSuperClasses = flag;
    }

    private class Visitor extends ClassVisitor {

        private final MethodVisitor methodVisitor = new MethodVisitor(ASM_VERSION) {
            @Override
            public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
                super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
                if (isMockableMethod(owner, name, descriptor)) {
                    register(owner, isInterface, name, descriptor);
                }
            }
        };

        protected Visitor() {
            super(ASM_VERSION);
        }

        @Override
        public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
            super.visit(version, access, name, signature, superName, interfaces);
            if (interfaces != null && visitInterfaces) {
                for (String interfaceName : interfaces) {
                    if (isMockableClass(interfaceName)) {
                        getOrCreateMockClass(interfaceName, true);
                    }
                }
            }
            if (superName != null && visitSuperClasses && isMockableClass(superName)) {
                getOrCreateMockClass(superName, true);
            }
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
            MethodVisitor original = super.visitMethod(access, name, descriptor, signature, exceptions);
            if (!visitMethods) {
                return original;
            }
            return methodVisitor;
        }
    }

    public void register(String owner, boolean isInterface, String name, String descriptor) {
        getOrCreateMockClass(owner, isInterface).register(name, descriptor);
    }

    public MockClass getOrCreateMockClass(String clazz, boolean isInterface) {
        if (clazz.startsWith("[L")) {
            clazz = clazz.substring(2, clazz.length() - 1);
        }
        return classes.computeIfAbsent(clazz, key -> new MockClass(key, isInterface));
    }

    public static class MockClass {

        private final ClassWriter writer;
        private final boolean isInterface;
        private final Set<String> methods = new HashSet<>();

        private final String clazz;

        public MockClass(String clazz, boolean isInterface) {
            this.clazz = clazz;
            this.isInterface = isInterface;
            this.writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
            int access = Opcodes.ACC_PUBLIC + Opcodes.ACC_SUPER;
            if (isInterface) {
                access += Opcodes.ACC_INTERFACE;
            }
            writer.visit(
                    JAVA_VERSION,
                    access,
                    clazz,
                    null,
                    "java/lang/Object",
                    null);
        }

        public void register(String name, String descriptor) {
            if (name.equals("<init>") && isInterface) {
                // Ignore interface constructors
                return;
            }
            String signature = name + descriptor;
            if (methods.contains(signature)) {
                return;
            }
            methods.add(signature);
            var visitor = writer.visitMethod(
                    Opcodes.ACC_PUBLIC,
                    name,
                    descriptor,
                    null,
                    null
            );
            if (!isInterface) {
                String message = "Cannot invoke a mocked method";
                if (name.equals("<init>")) {
                    // Call super constructor
                    visitor.visitVarInsn(Opcodes.ALOAD, 0);
                    visitor.visitMethodInsn(
                            Opcodes.INVOKESPECIAL,
                            "java/lang/Object",
                            "<init>",
                            "()V",
                            false
                    );
                    message = "Cannot instantiate mocked class";
                }
                // Throw new RuntimeException
                String exception = "java/lang/RuntimeException";
                visitor.visitTypeInsn(Opcodes.NEW, exception);
                visitor.visitInsn(Opcodes.DUP);
                visitor.visitLdcInsn(message);
                visitor.visitMethodInsn(
                        Opcodes.INVOKESPECIAL,
                        exception,
                        "<init>",
                        "(Ljava/lang/String;)V",
                        false
                );
                visitor.visitInsn(Opcodes.ATHROW);
            }
            visitor.visitMaxs(1, 1);
            visitor.visitEnd();
        }

    }

}
