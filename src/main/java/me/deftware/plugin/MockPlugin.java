package me.deftware.plugin;

import org.gradle.api.Plugin;
import org.gradle.api.Project;

public class MockPlugin implements Plugin<Project> {
    @Override
    public void apply(Project project) {
        project.getExtensions().create("mocking", MockExtension.class, project);
    }
}
