package me.deftware.plugin;

import groovy.lang.Closure;
import me.deftware.Mocker;
import org.gradle.api.Project;
import org.gradle.api.tasks.Input;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.jar.JarEntry;

import static me.deftware.Mocker.DELIMITER;

public class MockExtension {

    private static final String HASH_FILE = "mock.hash";
    private static final String CONFIGURATION_NAME = "compileOnly";

    private final Project target;
    private final File outDir;

    /**
     * Whether to visit methods called
     */
    private boolean visitMethods = true;
    /**
     * Whether to visit implemented interfaces
     */
    private boolean visitInterfaces = true;
    /**
     * Whether to visit extended super classes
     */
    private boolean visitSuperClasses = true;

    public MockExtension(Project target) {
        this.target = target;
        var homeDir = target.getGradle().getStartParameter().getGradleUserHomeDir();
        this.outDir = new File(homeDir, "mocks");
        if (!outDir.exists() && !outDir.mkdirs()) {
            target.getLogger().error("Failed to create mock directory");
        }
    }

    @Input
    public void mock(Closure<MockData> data) {
        var closure = new MockData(target);
        data.setDelegate(closure);
        data.setResolveStrategy(Closure.DELEGATE_FIRST);
        data.call(closure);
        process(closure);
    }

    @Input
    public void visitMethods(boolean flag) {
        this.visitMethods = flag;
    }

    @Input
    public void visitInterfaces(boolean flag) {
        this.visitInterfaces = flag;
    }

    @Input
    public void visitSuperClasses(boolean flag) {
        this.visitSuperClasses = flag;
    }

    private void process(MockData data) {
        File file = data.value;
        File mockFile = new File(outDir, file.getName());
        target.getDependencies().add(CONFIGURATION_NAME, target.files(file));
        try {
            if (!mockFile.exists() || !isUpToDate(data, mockFile)) {
                target.getLogger().lifecycle("Mocking " + file.getName());
                var mocker = new Mocker(file) {
                    @Override
                    public boolean isMockableMethod(String owner, String name, String descriptor) {
                        var result = isMockableClass(owner);
                        if (result) {
                            target.getLogger().lifecycle("Mocking {};{}{}", owner, name, descriptor);
                        }
                        return result;
                    }

                    @Override
                    public boolean isMockableClass(String owner) {
                        String namespace = owner.substring(0, owner.lastIndexOf('/'));
                        return data.classes.contains(owner) || data.packages.contains(namespace);
                    }
                };
                mocker.setVisitMethods(visitMethods);
                mocker.setVisitInterfaces(visitInterfaces);
                mocker.setVisitSuperClasses(visitSuperClasses);
                mocker.run();
                for (var method : data.custom) {
                    String[] parts = method.first.split(DELIMITER);
                    var mockClass = mocker.getOrCreateMockClass(parts[0], method.second);
                    if (parts.length == 2) {
                        String name = parts[1].substring(0, parts[1].indexOf('('));
                        String descriptor = parts[1].substring(parts[1].indexOf('('));
                        mockClass.register(name, descriptor);
                        target.getLogger().lifecycle("Mocking {};{}{}", parts[0], name, descriptor);
                    } else {
                        target.getLogger().lifecycle("Mocking standalone class {}", parts[0]);
                    }
                }
                int hashCode = data.hashCode();
                target.getLogger().lifecycle("Writing mock file with hash {} to {}", hashCode, mockFile);
                mocker.write(mockFile.toPath(), jarStream -> {
                    jarStream.putNextEntry(new JarEntry(HASH_FILE));
                    jarStream.write(toBytes(hashCode));
                    jarStream.closeEntry();
                });
            }
            target.getDependencies().add(CONFIGURATION_NAME, target.files(mockFile));
        } catch (Exception ex) {
            target.getLogger().error("Failed to mock " + file.getName(), ex);
        }
    }

    public boolean isUpToDate(MockData data, File target) throws IOException {
        // Extract the hash from the jar
        try (FileInputStream stream = new FileInputStream(target)) {
            var jar = new java.util.jar.JarInputStream(stream);
            var entry = jar.getNextJarEntry();
            while (entry != null) {
                if (entry.getName().equals(HASH_FILE)) {
                    byte[] bytes = jar.readAllBytes();
                    int hash = toInt(bytes);
                    if (hash != data.hashCode()) {
                        return false;
                    }
                }
                entry = jar.getNextJarEntry();
            }
        }
        // Check if the modification date of data.value is more recent than the target
        return data.value.lastModified() <= target.lastModified();
    }

    public static int toInt(byte[] bytes) {
        return (bytes[0] << 24)
                | ((bytes[1] & 0xFF) << 16)
                | ((bytes[2] & 0xFF) << 8)
                | (bytes[3] & 0xFF);
    }

    public static byte[] toBytes(int value) {
        return new byte[] {
                (byte) (value >>> 24),
                (byte) (value >>> 16),
                (byte) (value >>> 8),
                (byte) value
        };
    }

    public static class MockData {
        public File value;
        public List<String> classes = new ArrayList<>();
        public List<String> packages = new ArrayList<>();
        public List<Pair<String, Boolean>> custom = new ArrayList<>();

        private final Project project;

        public MockData(Project project) {
            this.project = project;
        }

        @Input
        public void value(File value) {
            this.value = value;
        }

        @Input
        public void value(String value) {
            var dependencyHandler = project.getDependencies();
            var configuration = project.getConfigurations().detachedConfiguration();
            var dependency = dependencyHandler.create(value);
            configuration.getDependencies().add(dependency);
            var files = configuration.resolve();
            // get the actual jar file
            for (var file : files) {
                if (file.getName().endsWith(".jar")) {
                    this.value = file;
                    break;
                }
            }
            if (this.value == null) {
                throw new IllegalArgumentException("Could not find jar file for " + value);
            }
        }

        @Input
        public void clazz(String clazz) {
            classes.add(clazz);
        }

        @Input
        public void namespace(String namespace) {
            packages.add(namespace);
        }

        @Input
        public void add(String method, boolean isInterface) {
            custom.add(new Pair<>(method, isInterface));
        }

        @Input
        public void add(String method) {
            add(method, false);
        }

        @Override
        public int hashCode() {
            // Create a hash based on the fields in this class.
            return Objects.hash(value, classes, packages, custom);
        }
    }

    public static class Pair<T, U> {
        private final T first;
        private final U second;

        public Pair(T first, U second) {
            this.first = first;
            this.second = second;
        }

        public T getFirst() {
            return first;
        }

        public U getSecond() {
            return second;
        }

        @Override
        public int hashCode() {
            return Objects.hash(first, second);
        }
    }

}
