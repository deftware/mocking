# Mocking

A Gradle plugin for mocking dependencies
required by compile-only dependencies.
Essentially, this eliminates specific
transient dependencies.

## Usage

If you have a project that requires the
file `baritone-1.6.2.jar` to compile, but
that file depends on something else that
isn't available, you can use this plugin.

For example, if `baritone-1.6.2.jar` depends
on `minecraft-1.16.5.jar`, you can use this
plugin to mock all Minecraft classes referenced,
thus allowing the usage of the jar without Minecraft.

```groovy
mocking {
    mock {
        value file("deps/baritone-1.6.2.jar")
        namespace "net/minecraft"
    }
}
```
